@echo off
:start
set HMSROOT=D:\HMS\HMSROOT\
set HMSSRC=D:\HMS\SOURCE\
set HMSINSTALL=D:\HMS\HMS Installation\

set HMSBASE=D:\HMS\HMS Grund

set VS=C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe
set BUILDLOG=buildlog.txt
set BUILDTEMP=%TEMP%\buildtemp

call:checkout https://peiturn@bitbucket.org/haglof_hms/bin.git "bin"
call:checkout https://peiturn@bitbucket.org/haglof_hms/chartdirector.git "ChartDirector"
call:checkout https://peiturn@bitbucket.org/haglof_hms/dbtransactionlib.git "DBTransactionLib"
call:checkout https://peiturn@bitbucket.org/haglof_hms/hmsfunclib.git "HMSFuncLib"
call:checkout https://peiturn@bitbucket.org/haglof_hms/include.git "Include"
call:checkout https://peiturn@bitbucket.org/haglof_hms/lib.git "lib"
call:checkout https://peiturn@bitbucket.org/haglof_hms/tinyxml.git "tinyxml"
call:checkout https://peiturn@bitbucket.org/haglof_hms/ziparchive.git "ZipArchive"
call:checkout https://peiturn@bitbucket.org/haglofpcteam/xml-reader.git "XML Reader"
call:checkout https://peiturn@bitbucket.org/haglofpcteam/xml-reader-sca.git "XML Reader SCA"
call:checkout https://peiturn@bitbucket.org/haglof_hms/hmsshell.git "HMSShell"
call:checkout https://peiturn@bitbucket.org/haglof_hms/logscalepc.git "LogScalePC"
call:checkout https://peiturn@bitbucket.org/haglof_hms/logscalevolumefunctions.git "LogScaleVolumeFunctions"
call:checkout https://peiturn@bitbucket.org/haglof_hms/module-versiobuilder.git "Module VersioBuilder
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-administration.git "Suite Administration"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-communication.git "Suite Communication"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-development.git "Suite Development"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-forrest.git "Suite Forrest"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-holmen.git "Suite Holmen"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-license.git "Suite License"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-reports.git "Suite Reports"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-reports2.git "Suite Reports2"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-sca.git "Suite Sca"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-timbercruise.git "Suite TimberCruise"
call:checkout https://peiturn@bitbucket.org/haglof_hms/suite-tims.git "Suite Tims"
call:checkout https://peiturn@bitbucket.org/haglof_hms/uccalculate.git "UCCalculate"
call:checkout https://peiturn@bitbucket.org/haglof_hms/uclandvaluenorm.git "UCLandValueNorm"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umcosts.git "UMCosts"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umdatabase.git "UMDataBase"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umdicup.git "UMDiCup"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umgis.git "UMGIS"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umestimate.git "UMEstimate"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umexchange.git "UMExchange"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umgallbas.git "UMGallBas"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umindata.git "UMIndata"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umkorus.git "UMKORUS"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umlandvalue.git "UMLandValue"
call:checkout https://peiturn@bitbucket.org/haglof_hms/ummarkoplant.git "UMMarkoPlant"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umpricelists.git "UMPricelists"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umrojning.git "UMRojning"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umscagallbas.git "UMScaGallBas"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umscasvupp.git "UMScaSvupp"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umstandtable.git "UMStandTable"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umtemplates.git "UMTemplates"
call:checkout https://peiturn@bitbucket.org/haglof_hms/umthinning.git "UMThinning"

if exist "%HMSROOT%" (rmdir /s /q "%HMSROOT%")
mkdir "%HMSROOT%"
mkdir "%HMSROOT%Language"
mkdir "%HMSROOT%Modules"
mkdir "%HMSROOT%Reports"
mkdir "%HMSROOT%Reports\ENU"
mkdir "%HMSROOT%Reports\SVE"
mkdir "%HMSROOT%Settings"
mkdir "%HMSROOT%Setup"
mkdir "%HMSROOT%Setup\ShellData"
mkdir "%HMSROOT%Suites"
copy /y "%HMSSRC%bin\*" "%HMSROOT%"
del %BUILDLOG%
echo Build started.
call:build "DBTransactionLib", "PAD_DBTransactionLib.sln"
call:build "HMSFuncLib", "HMSFuncLib.sln"
call:build "XML Reader", "XMLReader.sln"
call:build "XML Reader SCA", "XMLScaReader.sln"
call:build "HMSShell", "HMSShell.sln"
call:build "Suite Administration", "Adminstration.sln"
call:build "Suite Communication", "Suite Communication.sln"
call:build "Suite Development", "Suite Development.sln"
call:build "Suite Forrest", "Forrest.sln"
call:build "Suite Holmen", "Holmen.sln"
call:build "Suite License", "Suite License.sln"
call:build "Suite Reports", "Reports.sln"
call:build "Suite Reports2", "Reports2.sln"
call:build "Suite Sca", "SCA.sln"
call:build "Suite TimberCruise", "Landmark.sln"
call:build "Suite Tims", "Timms.sln"
call:build "LogScalePC", "LogScalePC.sln"
call:build "LogScaleVolumeFunctions", "LogScaleVolumeFunctions.sln"
call:build "Module VersioBuilder", "Module Versio Builder.sln"
call:build "UCCalculate", "UCCalculate.sln"
call:build "UCLandValueNorm", "UCLandValueNorm.sln"
call:build "UMCosts", "UMCosts.sln"
call:build "UMDataBase", "UMDataBase.sln"
call:build "UMDiCup", "UMDiCup.sln"
call:build "UMGIS", "GIS.sln"
call:build "UMEstimate", "UMEstimate.sln"
call:build "UMExchange", "UMExchange.sln"
call:build "UMGallBas", "UMGallBas.sln"
call:build "UMIndata", "UMIndata.sln"
call:build "UMKORUS", "UMKORUS.sln"
call:build "UMLandValue", "UMLandValue.sln"
call:build "UMMarkoPlant", "UMMarkoPlant.sln"
call:build "UMPricelists", "UMPricelists.sln"
call:build "UMRojning", "UMRojning.sln"
call:build "UMScaGallBas", "UMScaGallBas.sln"
call:build "UMScaSvupp", "UMScaSvupp.sln"
call:build "UMStandTable", "UMStandTable.sln"
call:build "UMTemplates", "UMTemplates.sln"
::call:build "UMThinning", "UMThinning.sln"

:: Error check
findstr /R /C:"[^0] failed," "%BUILDLOG%" > "%BUILDTEMP%"
for %%A in ("%BUILDTEMP%") do set size=%%~zA
if %size% GTR 0 (
  echo Build completed with errors.
) else (
  rmdir /s /q "%HMSINSTALL%"
  mkdir "%HMSINSTALL%"
  robocopy /e /z /r:3 /w:5 "%HMSBASE:~0,-1%" "%HMSINSTALL:~0,-1%"
  robocopy /e /z /r:3 /w:5 "%HMSROOT:~0,-1%" "%HMSINSTALL:~0,-1%"
  echo Done.
)
del "%BUILDTEMP%"

::start notepad "%BUILDLOG%"
goto:eof

:checkout
if EXIST "%HMSSRC%%~2" (
  git -C "%HMSSRC%%~2" reset --hard
  git -C "%HMSSRC%%~2" clean -fd
  git -C "%HMSSRC%%~2" pull
) else (
  git clone -b master --single-branch %~1 "%HMSSRC%%~2"
)
goto:eof

:build
echo %~1
"%VS%" "%HMSSRC%%~1\%~2" /build "Release|Win32" /out "%BUILDLOG%"
goto:eof
